package si.uni_lj.fri.pbd.miniapp2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MediaPlayerService extends Service {

    private static final String TAG = MediaPlayerService.class.getSimpleName();
    private static final int NOTIFICATION_ID = 22;
    public static final String ACTION_STOP = "stop_service";
    public static final String ACTION_START = "start_service";
    public static final String ACTION_PLAY = "play_service";
    public static final String ACTION_PAUSE = "pause_service";
    public static final String ACTION_EXIT = "exit_service";

    private static final String channelID = "background_timer";
    private final IBinder serviceBinder = new RunServiceBinder();
    private MediaPlayer player;
    private boolean isPlaying;
    private boolean isStopped;
    private int songNumber = 0;
    private int numberOfSongs = 3;
    private int[] songId = {R.raw.lucid_dreams,R.raw.only_you, R.raw.summertime_magic};
    private String[] songName = {"Lucid Dreams","Only You", "Summertime_magic"};
    private Handler handler = new UIUpdateHandler(this);
    private static final int MSG_UPDATE_TIME = 0;
    private boolean updateNotification = false;
    private boolean firstNotification = true;
    private AccelerationService accelerationService;
    private boolean serviceBound;
    private AccelerationBroadcastReceiver acb;

    // service connection
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Service bound");
            AccelerationService.RunServiceBinder binder = (AccelerationService.RunServiceBinder) iBinder;
            accelerationService = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "Service disconnect");
            serviceBound = false;
        }
    };

    //  gestures ON
    public void gesturesOn(){
        if(!serviceBound){
            Intent i =  new Intent(this,AccelerationService.class);
            i.setAction(AccelerationService.ACTION_START);
            startService(i);
            bindService(i, mConnection, 0);
        }
    }

    //  gestures OFF
    public void gesturesOff(){
        if(serviceBound){
            serviceBound = false;

            Intent i = new Intent(getApplicationContext(),AccelerationService.class);
            stopService(i);
            unbindService(mConnection);
        }

    }
    // broadcast receiver
    public class AccelerationBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("ACTION_ACC")){
                if(intent.getIntExtra("name",0) == 1){
                    Log.d("receiver","gesture-pause");
                    pauseButton();
                }else if(intent.getIntExtra("name",0) == 2){
                    Log.d("receiver","gesture-play");
                    startButton();
                }
            }
        }
    }


    public boolean isPlaying(){
        return isPlaying;
    }
    public boolean isStopped(){
        return isStopped;
    }

    //init
    @Override
    public void onCreate() {
        Log.d(TAG, "Creating service");
        // notification channel
        createNotificationChannel();
        //broadcast receiver
        acb = new AccelerationBroadcastReceiver();
        IntentFilter filter = new IntentFilter("ACTION_ACC");
        registerReceiver(acb,filter);

        isPlaying = false;
        isStopped = true;
        // mediaplayer
        player = MediaPlayer.create(this,R.raw.only_you);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playNextSong(mp);
            }
        });
    }

    // play button, if stopped - play next song
    public void startButton(){
        if(!isPlaying && !isStopped){
            player.start();
            isPlaying = true;
        }else if(isStopped){

            playNextSong(player);
            isStopped = false;
            isPlaying = true;
        }
    }

    // pause button - buggy if pausing at the same time as playNextSong() method
    public void pauseButton(){
        if(isPlaying){
            player.pause();
            isPlaying = false;
            Log.d("pause","pause1");
        }else{
            player.pause();
            Log.d("pause","pause2");
        }
    }

    //stop button
    public void stopButton(){
        player.stop();

        isPlaying = false;
        isStopped = true;
    }

    // play next song
    public void playNextSong(MediaPlayer mp){
        try {
            songNumber = (songNumber+1)%numberOfSongs;
            AssetFileDescriptor afd = getApplicationContext().getResources().openRawResourceFd(songId[songNumber]);
            if (afd == null) {
                Toast.makeText(getApplicationContext(),"playback error",Toast.LENGTH_LONG).show();
            }else{
                mp.reset();
                mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                mp.prepare();
                mp.start();
            }
        } catch (IOException | IllegalArgumentException | SecurityException ex) {
            Log.d("TAG", "create failed:", ex);
        }
    }

    // return song name
    public String getSongInfo(){
        return songName[songNumber];
    }

    //return song duration
    public String getSongDuration(){
        SimpleDateFormat df = new SimpleDateFormat("mm:ss");
        String duration = df.format(new Date(player.getDuration()));
        String position = df.format(new Date(player.getCurrentPosition()));
        return position +"/"+ duration;
    }


    // starting
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting service");

        if(intent.getAction() == ACTION_STOP){
            stopButton();
            isStopped = true;
        }else if(intent.getAction() == ACTION_PLAY){
            isStopped = false;
            startButton();
        }else if(intent.getAction() == ACTION_PAUSE){
            pauseButton();
        }else if(intent.getAction() == ACTION_EXIT){
            stopForeground(true);
            stopSelf();
        }

        return Service.START_STICKY;
    }


    //binder
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");
        return serviceBinder;
    }

    // cleanup
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(updateNotification){
            handler.removeMessages(MSG_UPDATE_TIME);
        }
        unregisterReceiver(acb);

        gesturesOff();

        player.release();
        stopSelf();
        Log.d(TAG, "Destroying service");
    }

    class RunServiceBinder extends Binder {
        MediaPlayerService getService(){
            return MediaPlayerService.this;
        }
    }

    //start notifications
    public void foreground(){
        startForeground(NOTIFICATION_ID,createNotification());
        handler.sendEmptyMessage(MSG_UPDATE_TIME);
        updateNotification = true;
    }
    //stop notifications
    public void background(){
        handler.removeMessages(MSG_UPDATE_TIME);
        stopForeground(true);
        updateNotification = false;
    }

    public void updateUITimer(){
        createNotification();
    }

    // handles updates to notifications
    static class UIUpdateHandler extends Handler {

        private final static int UPDATE_RATE_MS = 1000;
        private final WeakReference<MediaPlayerService> activity;

        UIUpdateHandler(MediaPlayerService activity) { this.activity = new WeakReference<>(activity); }

        @Override
        public void handleMessage(Message message) {
            if (MSG_UPDATE_TIME == message.what) {
                activity.get().updateUITimer();
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    }


    private NotificationManager mNotificationManager;
    // creating and updating notification
    // max 3 buttons on notification
    // it's ugly but it works - should refactor
    private Notification createNotification() {
        if(firstNotification){
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if(isStopped){

            //Log.d("notification","notification1");

            Intent actionIntent = new Intent(this, MediaPlayerService.class);
            actionIntent.setAction(ACTION_PLAY);
            PendingIntent actionPendingIntent = PendingIntent.getService(this,0,actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent actionIntent1 = new Intent(this, MediaPlayerService.class);
            actionIntent1.setAction(ACTION_PAUSE);
            PendingIntent actionPendingIntent1 = PendingIntent.getService(this,0,actionIntent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent actionIntent2 = new Intent(this, MediaPlayerService.class);
            actionIntent2.setAction(ACTION_EXIT);
            PendingIntent actionPendingIntent2 = PendingIntent.getService(this,0,actionIntent2, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelID)
                    .setContentTitle("(SongInfo)")
                    .setContentText("(SongDuration)")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(channelID)
                    .addAction(android.R.drawable.ic_media_play,"Play",actionPendingIntent)
                    .addAction(android.R.drawable.ic_media_pause,"Pause",actionPendingIntent1)
                    .addAction(android.R.drawable.ic_menu_close_clear_cancel,"Exit",actionPendingIntent2);

            Intent resultIntent = new Intent(this, MainActivity.class);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(this, 0, resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            Notification tmp = builder.build();
            if(!firstNotification){
                mNotificationManager.notify(NOTIFICATION_ID, tmp);

                //Log.d("notification","notification1.1");
            }else{
                //Log.d("notification","notification1.2");
            }
            firstNotification = false;
            return tmp;
        }else if(isPlaying){

            //Log.d("notification","notification2");
            Intent actionIntent = new Intent(this, MediaPlayerService.class);
            actionIntent.setAction(ACTION_STOP);
            PendingIntent actionPendingIntent = PendingIntent.getService(this,0,actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent actionIntent1 = new Intent(this, MediaPlayerService.class);
            actionIntent1.setAction(ACTION_PAUSE);
            PendingIntent actionPendingIntent1 = PendingIntent.getService(this,0,actionIntent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent actionIntent2 = new Intent(this, MediaPlayerService.class);
            actionIntent2.setAction(ACTION_EXIT);
            PendingIntent actionPendingIntent2 = PendingIntent.getService(this,0,actionIntent2, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelID)
                    .setContentTitle(getSongInfo())
                    .setContentText(getSongDuration())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(channelID)
                    .addAction(android.R.drawable.button_onoff_indicator_off,"Stop",actionPendingIntent)
                    .addAction(android.R.drawable.ic_media_pause,"Pause",actionPendingIntent1)
                    .addAction(android.R.drawable.ic_menu_close_clear_cancel,"Exit",actionPendingIntent2);

            Intent resultIntent = new Intent(this, MainActivity.class);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(this, 0, resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            Notification tmp = builder.build();
            if(!firstNotification){
                mNotificationManager.notify(NOTIFICATION_ID, tmp);

                //Log.d("notification","notification2.1");
            }else{
                //Log.d("notification","notification2.2");
            }
            firstNotification = false;
            return tmp;
        }else{

            //Log.d("notification","notification3");
            Intent actionIntent = new Intent(this, MediaPlayerService.class);
            actionIntent.setAction(ACTION_PLAY);
            PendingIntent actionPendingIntent = PendingIntent.getService(this,0,actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent actionIntent1 = new Intent(this, MediaPlayerService.class);
            actionIntent1.setAction(ACTION_PAUSE);
            PendingIntent actionPendingIntent1 = PendingIntent.getService(this,0,actionIntent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent actionIntent2 = new Intent(this, MediaPlayerService.class);
            actionIntent2.setAction(ACTION_EXIT);
            PendingIntent actionPendingIntent2 = PendingIntent.getService(this,0,actionIntent2, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelID)
                    .setContentTitle(getSongInfo())
                    .setContentText(getSongDuration())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(channelID)
                    .addAction(android.R.drawable.ic_media_play,"Play",actionPendingIntent)
                    .addAction(android.R.drawable.ic_media_pause,"Pause",actionPendingIntent1)
                    .addAction(android.R.drawable.ic_menu_close_clear_cancel,"Exit",actionPendingIntent2);

            Intent resultIntent = new Intent(this, MainActivity.class);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(this, 0, resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            Notification tmp = builder.build();
            if(!firstNotification){
                mNotificationManager.notify(NOTIFICATION_ID, tmp);
                //Log.d("notification","notification3.1");
            }else{
                //Log.d("notification","notification3.2");
            }
            firstNotification = false;
            return tmp;
        }
    }

    // Notification Channel
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        } else {
            NotificationChannel channel = new NotificationChannel(MediaPlayerService.channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(getString(R.string.channel_desc));
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            NotificationManager managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            managerCompat.createNotificationChannel(channel);
        }
    }
}

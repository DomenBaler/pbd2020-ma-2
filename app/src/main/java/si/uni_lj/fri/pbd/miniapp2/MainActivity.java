package si.uni_lj.fri.pbd.miniapp2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private MediaPlayerService playerService;
    private boolean serviceBound;
    private TextView songInfo;
    private TextView songDuration;
    private final Handler mUpdateTimeHandler = new UIUpdateHandler(this);
    private final static int MSG_UPDATE_TIME = 0;


    // new Service Connection
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "MediaPlayerService bound");

            MediaPlayerService.RunServiceBinder binder = (MediaPlayerService.RunServiceBinder) iBinder;
            playerService = binder.getService();

            serviceBound = true;
            if(!playerService.isStopped()){
                updateUIStartRun();
            }
            playerService.background();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "MediaPlayerService disconnected");
            serviceBound = false;
        }
    };


    // getting textViews for updating UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songInfo = (TextView) findViewById(R.id.songInfo);
        songDuration = (TextView) findViewById(R.id.songDuration);
    }


    //start and bind MediaPlayerService, if restored from notification --> start updating UI
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Starting and binding MediaPlayerService");
        Intent i =  new Intent(this,MediaPlayerService.class);
        i.setAction(MediaPlayerService.ACTION_START);
        startService(i);
        bindService(i, mConnection, 0);

        if(serviceBound){
            updateUIStartRun();
        }
    }

    // MediaPlayerService --> foreground ,stop updating UI in activity
    @Override
    protected void onStop() {
        super.onStop();

        updateUIStopRun();
        if(serviceBound){
            playerService.foreground();
        }
    }

    // Play Button
    public void playButtonClick(View v){
        playerService.startButton();
        updateUIStartRun();
    }
    // Pause Button
    public void pauseButtonClick(View v){
        playerService.pauseButton();
    }
    // Stop Button
    public void stopButtonClick(View v){
        playerService.stopButton();
        updateUIStopRun();
    }
    // stop service and exit
    public void exitButtonClick(View v){
        Intent i = new Intent(getApplicationContext(),MediaPlayerService.class);
        stopService(i);
        unbindService(mConnection);

        finishAffinity();
        System.exit(0);
    }
    //gestures ON
    public void gesturesOnButtonClick(View v){
        Toast.makeText(this,"Gestures - ON",Toast.LENGTH_LONG).show();
        playerService.gesturesOn();
    }
    //gestures OFF
    public void gesturesOffButtonClick(View v){
        Toast.makeText(this,"Gestures - OFF",Toast.LENGTH_LONG).show();
        playerService.gesturesOff();
    }


    //start updating UI
    private void updateUIStartRun() {
        mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
    }
    // update UI
    private void updateUIStopRun() {
        mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);

        // update song info and duration
        songInfo.setText("(Song info)");
        songDuration.setText("(Song duration)");
    }
    //stop updating UI
    private void updateUITimer() {
        songInfo.setText(playerService.getSongInfo());
        songDuration.setText(playerService.getSongDuration());
    }

    //handles updates to UI in Activity
    static class UIUpdateHandler extends Handler {

        private final static int UPDATE_RATE_MS = 1000;
        private final WeakReference<MainActivity> activity;
        UIUpdateHandler(MainActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message message) {
            if (MSG_UPDATE_TIME == message.what) {
                activity.get().updateUITimer();
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    }
}


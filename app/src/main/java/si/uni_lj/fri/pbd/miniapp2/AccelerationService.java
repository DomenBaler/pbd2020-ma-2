package si.uni_lj.fri.pbd.miniapp2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;

public class AccelerationService extends Service implements SensorEventListener {

    private final IBinder serviceBinder = new RunServiceBinder();
    public static final String ACTION_START = "start_service";
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private static final int NOISE_THRESHOLD = 5;
    private float x,y,z,x1,y1,z1,dx,dy,dz;
    private boolean firstRead;



    // binder
    class RunServiceBinder extends Binder {
        AccelerationService getService(){
            Log.d("tt","tt");
            return AccelerationService.this;
        }
    }


    // init
    @Override
    public void onCreate() {
        super.onCreate();

        x = 0;
        y = 0;
        z = 0;
        x1 = 0;
        y1 = 0;
        z1 = 0;
        dx = 0;
        dy = 0;
        dz = 0;

        firstRead = true;

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this,accelerometer,SensorManager.SENSOR_DELAY_UI);

    }


    // cleanup - unregister Listener
    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("ddd","ddd");
        return serviceBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(firstRead){
            //set Default values
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            firstRead = false;
        }else{
            // read values and update values
             x1 = event.values[0];
             y1 = event.values[1];
             z1 = event.values[2];

             dx = Math.abs(x-x1);
             dy = Math.abs(y-y1);
             dz = Math.abs(z-z1);

             x = x1;
             y = y1;
             z = z1;
             // calculate change
             if(dx < NOISE_THRESHOLD){
                 dx = 0;
             }
             if(dy < NOISE_THRESHOLD){
                 dy = 0;
             }
             if(dz < NOISE_THRESHOLD){
                 dz = 0;
             }
            // if change detected --> send broadcast
             if(dx > dz){
                 Intent intent = new Intent("ACTION_ACC");
                 intent.putExtra("name",1);
                 sendBroadcast(intent);
             }else if( dz > dx){
                 Intent intent = new Intent("ACTION_ACC");
                 intent.putExtra("name",2);
                 sendBroadcast(intent);
             }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
